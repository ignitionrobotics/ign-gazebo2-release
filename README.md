# ign-gazebo2-release

The ign-gazebo2-release repository has moved to: https://github.com/ignition-release/ign-gazebo2-release

Until May 31st 2020, the mercurial repository can be found at: https://bitbucket.org/osrf-migrated/ign-gazebo2-release
